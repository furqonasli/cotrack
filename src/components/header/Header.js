import React from 'react'
import { View, Text, Image } from 'react-native'

const Header = (props) => {
    return (
        <View style={{
            width:'100%',
            height: 200,
            backgroundColor: '#2F0092',
            borderBottomLeftRadius:15,
            borderBottomRightRadius:15,
            paddingHorizontal:7,
        }}
        >
        <View opacity={.2} style={{backgroundColor: '#6E54B9', width: 200, height: 200, borderRadius: 100, position:'absolute', right:-50,top:-20}} />
        <View opacity={.3} style={{backgroundColor: '#6E54B9', width: 150, height: 150, borderRadius: 100, position:'absolute', right:100,top:-70}} />
        <Text style={{color:'#fff', fontSize: 24, fontWeight:'700',marginTop:40}} >COVID TRACKING</Text>
        <Text style={{color:'#fff', fontSize: 14, fontWeight:'700',marginTop:3}} >{props.subtitle}</Text>
        <Image source={require('../../assets/headerIndo.png')} style={{width:180,height:180,right:-15,position:'absolute'}} />
        </View>
    )
}

export default Header
