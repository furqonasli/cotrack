import React from 'react'
import { View, Text, Image } from 'react-native'
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

const LoadingMainInfo = (props) => {
    return (
        <View style={{
            width:'100%',
            height:90,
            backgroundColor:'#fff',
            borderRadius:5,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 3,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,
            elevation: 6,
            justifyContent:'center',
            alignItems:'center',
            marginBottom:10,
            flexDirection:'row'
        }} >
            <View style={{flex:1, justifyContent:'center',alignItems:'center'}} >
            <SkeletonPlaceholder>
                <SkeletonPlaceholder.Item >
                <SkeletonPlaceholder.Item width={120} height={13} borderRadius={4} />
                <SkeletonPlaceholder.Item width={100} height={15} borderRadius={4} marginTop={5} />
                <SkeletonPlaceholder.Item width={100} height={10} borderRadius={4} marginTop={5} />
                </SkeletonPlaceholder.Item>
            </SkeletonPlaceholder>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                <Image style={{width:200,height:110,position:'absolute',right:10,top:-65}} source={require('../../assets/stats2.png')} />
            </View>
        </View>
    )
}

export default LoadingMainInfo
