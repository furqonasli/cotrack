import React from 'react'
import { View, Text, ScrollView } from 'react-native'

const CardInfo = (props) => {
    return (
        <View style={{
            flex:1,
            backgroundColor:'#fff',
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 3,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,
            elevation: 6,
            justifyContent:'center',
            alignItems:'center',
            marginLeft:props.ml,
            marginRight:props.mr,
            borderRadius:5,
        }} >
        <Text style={{color:props.warna, fontWeight:'700',fontSize:props.fontSize}} >{props.title}</Text>
        <Text style={{color:props.warna, fontWeight:'700',fontSize:20}} >{props.nilai}</Text>
        </View>
    )
}

export default CardInfo
