import React from 'react'
import { View, Text, Image } from 'react-native'

const CardInfoIndo = (props) => {
    return (
        <View style={{flexDirection:'row',marginBottom:10}}>
            <View style={{flex:1.5}}>
                <Text style={{fontSize:16,fontWeight:'bold',color:props.warna,marginBottom:-5}}>
                    TOTAL {props.title}
                </Text>
                <Text style={{fontSize:30,fontWeight:'bold',color:'#838383',marginBottom:-5}}>
                    {props.nilai}
                </Text>
                <Text style={{fontSize:14,fontWeight:'bold',color:'#838383',marginBottom:-5}}>
                    ORANG
                </Text>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'center',}}>
                <Image style={{width:50,height:50}} source={props.img} />
            </View>
        </View>
    )
}

export default CardInfoIndo
