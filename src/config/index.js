import axios from 'axios';

const apiIndo = axios.create({
  baseURL: 'https://api.kawalcorona.com/',
});
const apiGlobal = axios.create({
  baseURL: 'https://covid19.mathdro.id/api/',
});

const apiNews = axios.create({
  baseURL: 'https://newsapi.org/v2/top-headlines?country=id&category=health&apiKey=77ab58c5ba48412f9f2174e2b2d2c63d',
});

export {
  apiIndo,
  apiNews,
  apiGlobal
} ;
