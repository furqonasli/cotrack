import React from 'react'
import { View } from 'react-native'
import CardIntruksi from '../components/cardIntruksi/CardIntruksi'


const CardIntruksiMol = () => {
    return (
        <View style={{flex:1,paddingHorizontal:20,marginTop:15}}>
            <CardIntruksi 
                img={require('../assets/imune.png')}
                mt={-80} mr={40}
                w={145} h={145}
                title={"Meningkatkan Imune Tubuh"} deskripsi={"Imune tubuh sangat berperan penting untuk melindungi tubuh kita dari serangan covid-19. Dengan imune tubuh yang kuat, tubuh kita jadi tidak mudak untuk terjangkit virus dan penyakit lainnya."}
                warnaBg={'#FDB5ED'} warnaTitle={'#FF00C7'} warnaDes={'#808080'}
            />
            <CardIntruksi 
                img={require('../assets/masker.png')}
                mt={-100} mr={40}
                w={170} h={120}
                title={"Menggunakan Masker"} deskripsi={"Pakialah masker untuk melindungi penularan Covid-19. Dengan menggunakan masker akan mengurangi resiko ketularan Covid-19"}
                warnaBg={'#CCE6FA'} warnaTitle={'#00B2FF'} warnaDes={'#808080'}
            />
            <CardIntruksi 
                img={require('../assets/handSanitizer.png')}
                mt={-120} mr={40}
                w={130} h={90}
                title={"Menggunakan Hand Sanitizer"} deskripsi={"Pada saat beraktivitas, banyak kuman atau virus yang menempel ditangan. Maka dari itu gunakanlah Hand Sanitizer untuk membunuh kuman tersebut."}
                warnaBg={'#CDFFBC'} warnaTitle={'#00CB2D'} warnaDes={'#808080'}
            />
            <CardIntruksi 
                img={require('../assets/jagaJarak2.png')}
                mt={-110} mr={40}
                w={160} h={110}
                title={"Menjaga Jarak"} deskripsi={"Menjaga saat bertemu ataupun berbicara dengan orang lain, hal itu untuk mencegah penularan Covid-19 pada tubuh kita"}
                warnaBg={'#FFEA9F'} warnaTitle={'#BD8800'} warnaDes={'#808080'}
            />
            <CardIntruksi 
                img={require('../assets/cuciTangan.png')}
                mt={-100} mr={40}
                w={120} h={120}
                title={"Mencuci Tangan"} deskripsi={"Mecuci tangan setiap saat bisa membunuh kuman yang menempel di tangan kita. Hal ini menjadi salah satu cara yang efektif untuk mencegah penularan Covid-19"}
                warnaBg={'#D0FFF9'} warnaTitle={'#00B2FF'} warnaDes={'#808080'}
            />
        </View>
    )
}

export default CardIntruksiMol
