import React, { Component } from 'react'
import { ScrollView, Text, View } from 'react-native'
import CardProv from '../../components/cardProv/CardProv'
import Header from '../../components/header/Header'
import LoadingIndo from '../../components/loadings/LoadingIndo'
import CardInfoIndo from '../../components/mainCardInfo/CardInfoIndo'
import { apiIndo } from '../../config'



export class IndonesiaScreen extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             data: [],
             dataProv: [],
             loading: true
        }
    }

    componentDidMount(){
        this.getData()
        this.getDataProv()
    }

    getData = () => {
        this.setState({loading: true})
        apiIndo.get('indonesia').then(hasil => {
            this.setState({
                data: hasil.data,
                loading: false
            })
        }).catch((e) => {
            this.setState({loading: true})
        })
    }

    getDataProv = () =>{
        apiIndo.get('indonesia/provinsi').then(hasil => {
            console.log(hasil.data);
            this.setState({dataProv: hasil.data})
        }).catch((e) => {
            console.error(e);
        })
    }
    
    render() {
        const {data,loading,dataProv} = this.state;
        return (
            <ScrollView showsVerticalScrollIndicator={false} style={{backgroundColor:'#fff'}}>
                <View>
                    <Header subtitle={"Indonesia Data"} />
                </View>
                {/* Card Info */}
                <View style={{flex:1,height:230,alignItems:'center',}}>
                    <View style={{
                        width:'90%',
                        height:250,
                        backgroundColor:'#fff',
                        borderRadius:5,
                        marginTop:-40,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 2,
                            height: 2,
                        },
                        shadowOpacity: 0.27,
                        shadowRadius: 4.65,
                        elevation: 6,
                        paddingVertical:10,
                        paddingHorizontal:15,
                        marginBottom:10
                    }}>
                        {/* text data */}
                        {
                            loading ? 
                            <View>
                                <LoadingIndo />
                                <LoadingIndo />
                                <LoadingIndo />
                            </View>
                            :
                            <View>
                                {data.map((item,key) => {
                                    return(
                                        <View key={key}>
                                            <CardInfoIndo title={"MENINGGAL"} warna={'#FF0000'} nilai={item.meninggal} img={require('../../assets/Meninggal.png')} />
                                            <CardInfoIndo title={"POSITIF"} warna={'#FF9900'} nilai={item.positif} img={require('../../assets/Positif.png')} />
                                            <CardInfoIndo title={"SEMBUH"} warna={'#00BB34'} nilai={item.sembuh} img={require('../../assets/Sembuh.png')} />
                                        </View>
                                    )
                                })}
                            </View>
                        }
                    </View>
                </View>
                {/* Text */}
                <View style={{flex:1,paddingHorizontal:20}}>
                    <Text style={{
                        fontSize:22,fontWeight: "700",color:'#3A1B7A',}} >
                        PROVINSI
                    </Text>
                    <Text style={{
                        fontSize:15,color:'#929292',}} >
                        Persebaran Covid-19 di Indonesia sudah 
                        sangat luas, berikut data persebaran Covid-19
                        di berbagai Provinsi yang ada di Indonesia. 
                    </Text>
                </View>
                {/* data provinsi */}
                <View style={{flex:1,paddingHorizontal: 20,marginVertical:10,}}>
                    {dataProv.map((item,key) => {
                        return(
                            <View key={key}>
                                <CardProv prov={item.attributes.Provinsi} />
                            </View>
                        )
                    })}
                </View>
            </ScrollView>
        )
    }
}

export default IndonesiaScreen
