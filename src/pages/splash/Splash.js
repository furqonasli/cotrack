import React, {useEffect} from 'react';
import { View, Image } from 'react-native';

const Splash = ({navigation}) => {

    useEffect(() => {
        setTimeout(() => {
          navigation.replace('GlobalScreen');
        }, 2000);
      });
    

    return (
        <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
            <Image style={{width:170,height:75}} source={require('../../assets/Splash2.png')} />
        </View>
    )
}

export default Splash
